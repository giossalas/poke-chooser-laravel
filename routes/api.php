<?php

use App\Http\Controllers\api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>'only.json'],function(){
    Route::post('/getUserWithPokemons',[UserController::class,'userWithPokemons']);
    Route::post('/createUser',[UserController::class,'create']);
    Route::post('/saveUserPokemons',[UserController::class,'saveUserPokemons']);
});
