<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPokemons extends Model
{
    use HasFactory;
    protected $table ="user_pokemons";
    protected $fillable=['user_id','number_pokedex'];
    public $timestamps = false;
}
