<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\api\User;
use App\Models\api\UserPokemons;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public function create(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'user'=> 'required|unique:users|regex:/^[a-z]+$/i|min:5|max:200'
        ]);
        if($validator->fails()){
            if(isset($validator->failed()['user']['Required'])){
                return response()->json(['message'=>'Debes ingresar un usuario'],400);
            }
            if(isset($validator->failed()['user']['Unique'])){
                return response()->json(['message'=>'El usuario ya existe, intenta uno direfente'],419);
            }
            if(isset($validator->failed()['user'])){
                return response()->json(['message'=>'El usuario no es válido, solo se permiten letras [ Entre 5 y 200 caracteres ]'],419);
            }
        }

        $user = User::create(['user'=>$request->user]);
        return response()->json(['user'=>$user,'message'=>'Usuario creado']);
    }

    public function userWithPokemons(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user'=> 'required|string|exists:users'
        ]);
        if($validator->fails()){
            if(isset($validator->failed()['user']['Required'])){
                return response()->json(['message'=>'Debes ingresar un usuario'],400);
            }
            if(isset($validator->failed()['user']['String'])){
                return response()->json(['message'=>'El usuario debe ser texto'],419);
            }
            if(isset($validator->failed()['user']['Exists'])){
                return response()->json(['message'=>'El usuario no existe'],419);
            }
        }
        $user = User::where(['user'=>$request->user])->first();
        $pokemons=UserPokemons::where(['user_id'=>$user->id])->get('number_pokedex');

        return response()->json(['user'=>$user,'pokemons'=>$pokemons]);
    }

    public function saveUserPokemons(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user'=> 'required|string|exists:users',
            'pokemons' => [
                'required',
                function($attribute, $value, $fail){
                    if(!is_array($value)){
                        $fail("La lista de pokemones [$attribute] no es válida");
                    }
                }
            ]
        ]);
        if($validator->fails()){
            if(isset($validator->failed()['user']['Required'])){
                return response()->json(['message'=>'Debes ingresar un usuario'],400);
            }
            if(isset($validator->failed()['user']['String'])){
                return response()->json(['message'=>'El usuario debe ser texto'],419);
            }
            if(isset($validator->failed()['user']['Exists'])){
                return response()->json(['message'=>'El usuario no existe'],419);
            }
            if(isset($validator->failed()['pokemons']['Required'])){
                return response()->json(['message'=>'Debes seleccionar una lista de pokemones'],400);
            }
            if(isset($validator->failed()['pokemons'])){
                return response()->json(['message'=>'La lista de pokemones no es válida'],419);
            }
        }
        $user = User::where(['user'=>$request->user])->first();
        $data=[];
        foreach($request->pokemons as $pokemon){
            $data[]=[
                'user_id'=>$user->id,
                'number_pokedex'=>$pokemon
            ];
        }
        DB::beginTransaction();
        try{
            UserPokemons::where(['user_id'=>$user->id])->delete();
            UserPokemons::insert($data);
            DB::commit();
        }
        catch(Exception $e){
            DB::rollBack();
            return response()->json(["message"=>'No se pudo realizar la inserción. E='.$e->getMessage()],419);
        }

        return response()->json(['user'=>$user,'message'=>'Pokemones guardados']);
    }
}
