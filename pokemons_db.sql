
CREATE DATABASE pokemons_db;
USE pokemons_db;

CREATE DATABASE IF NOT EXISTS `pokemons_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pokemons_db`;


DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_UNIQUE` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


INSERT INTO `users` (`id`, `user`) VALUES
(1, 'esteban'),
(3, 'estebandos'),
(2, 'romualdo');


DROP TABLE IF EXISTS `user_pokemons`;
CREATE TABLE IF NOT EXISTS `user_pokemons` (
  `id` int NOT NULL AUTO_INCREMENT,
  `number_pokedex` int NOT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;



INSERT INTO `user_pokemons` (`id`, `number_pokedex`, `user_id`) VALUES
(9, 1, 1),
(10, 2, 1),
(11, 3, 1),
(12, 4, 1),
(13, 5, 1),
(14, 6, 1),
(21, 10, 2),
(22, 9, 2),
(23, 8, 2),
(24, 7, 2),
(25, 6, 2),
(26, 3, 2),
(27, 2, 3),
(28, 3, 3),
(29, 9, 3),
(30, 10, 3),
(31, 4, 3),
(32, 5, 3);


ALTER TABLE `user_pokemons`
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
COMMIT;
